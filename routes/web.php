<?php
//  namespace App\Http\Controllers;

use App\Http\Controllers\Admin\AdminChangePasswordController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdminprofileController;
use App\Http\Controllers\Admin\CreateDriverController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CarService\CarServiceController;
use App\Http\Controllers\Driver\DriverController;
use App\Http\Controllers\Passenger\PassengerController;
use App\Http\Controllers\SuperAdmin\SuperAdminControoller;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CarService\CarserviceProfile;
use App\Http\Controllers\CarService\CarserviceProfileController;
use App\Http\Controllers\CreateAdminController;
use App\Http\Controllers\Driver\DriverProfileController;
use App\Http\Controllers\Passenger\PassengerprofileController;
use App\Http\Controllers\SuperAdmin\ChanngePasswordController;
use App\Http\Controllers\Admin\ChangePasswordController;
use App\Http\Controllers\CarService\AddcarController;
use App\Http\Controllers\CarService\CarserviceChangePasswordController;
use App\Http\Controllers\Driver\DriverChangePasswordController;
use App\Http\Controllers\Passenger\PassengerChangePasswordController;
use App\Http\Controllers\SuperAdmin\SuperadminprofileController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|----------------------------------FFF----------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!

*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['verify' => true]);

Route::get('logout', [LoginController::class, 'logout'])->name('logout');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['as' => 'superadmin.', 'prefix' => 'superadmin', 'middleware' => ['auth', 'superadmin']], function () {
    Route::get('/', [SuperAdminControoller::class, 'index']);
    Route::get('dashboard', [SuperAdminControoller::class, 'index'])->name('dashboard');
    Route::resource('profile', SuperadminprofileController::class);
    Route::get('delete/{id}', [SuperadminprofileController::class, 'destroy']);
    Route::resource('createadmin', CreateAdminController::class);
    Route::resource('password_update', ChanngePasswordController::class);
});


Route::group(
    ['as' => 'admin.', 'prefix' => 'admin', 'middleware' => ['auth', 'admin']],
    function () {
        Route::get('dashboard', [AdminController::class, 'index'])->name('dashboard');
        Route::resource('profile', AdminprofileController::class);
        Route::get('delete/{id}', [AdminprofileController::class, 'destroy']);
        Route::resource('createdriver', CreateDriverController::class);
        Route::resource('password_update', AdminChangePasswordController::class);
    }
);



Route::group(['as' => 'passenger.', 'prefix' => 'passenger', 'middleware' => ['auth', 'passenger']], function () {
    Route::get('dashboard', [PassengerController::class, 'index'])->name('dashboard');
    Route::resource('profile', PassengerprofileController::class);
    Route::get('delete/{id}', [PassengerprofileController::class, 'destroy']);
    Route::resource('password_update', PassengerChangePasswordController::class);
});


Route::group(['as' => 'driver.', 'prefix' => 'driver',  'middleware' => ['auth', 'driver']], function () {
    Route::get('dashboard', [DriverController::class, 'index'])->name('dashboard');
    Route::resource('profile', DriverProfileController::class);
    Route::get('delete/{id}', [DriverProfileController::class, 'destroy']);
    Route::resource('password_update', DriverChangePasswordController::class);
});




Route::group(['as' => 'carservice.', 'prefix' => 'carservice', 'middleware' => ['auth', 'carservice']], function () {
    Route::get('dashboard', [CarServiceController::class, 'index'])->name('dashboard');
    Route::resource('profile', CarserviceProfileController::class);
    Route::get('delete/{id}', [CarserviceProfileController::class, 'destroy']);
    Route::resource('password_update', CarserviceChangePasswordController::class);
    Route::resource('addcar', AddcarController::class);
});
