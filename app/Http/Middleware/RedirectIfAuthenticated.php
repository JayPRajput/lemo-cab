<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        // if(session()->get("url.intended")){
        //     session()->put("redirect_after_email_verification", session()->get("url.intended"));
        // }

        if (Auth::guard($guard)->check() && Auth::user()->role_id == 1) {
            return redirect()->route('superadmin.dashboard');
        } elseif (Auth::guard($guard)->check() && Auth::user()->role_id == 2) {
            return redirect()->route('admin.dashboard');
        } elseif (Auth::guard($guard)->check() && Auth::user()->role_id == 3) {
            return redirect()->route('passenger.dashboard');
        } elseif (Auth::guard($guard)->check() && Auth::user()->role_id == 4) {
            return redirect()->route('driver.dashboard');
        } elseif (Auth::guard($guard)->check() && Auth::user()->role_id == 5) {
            return redirect()->route('carservice.dashboard');
        } else {
            return $next($request);
        }
    }
}
