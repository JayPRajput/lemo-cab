<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class usercontroller extends Controller
{
    public function index()
    {
        $data = User::select('id', 'role_id', 'name', 'email')->get();

        if ($data->isEmpty()) {
            return response()->json(['error' => 'data is not available']);
        }
         return response()->json(['data' => $data]);
    }
}
