<?php

namespace App\Http\Controllers\CarService;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Caradmin;
use Illuminate\Support\Facades\Auth;

class AddcarController extends Controller
{
    public function index()
    {
        $car = Caradmin::get();
        // dd($car);
        return view('carservice.addcar', compact('car'));
    }

    public function store(Request $request)
    {
        $cars = new Caradmin();
        if ($request->has('car_image')) {
            $file =  $request->file('car_image');
            $filename =  time() . '.' . $file->getClientOriginalExtension();
            $file->move('uploads/cars/', $filename);
            $cars->car_image =  $filename;
        }

        $cars->car_name = $request->car_name;
        $cars->car_model = $request->car_model;
        $cars->car_number = $request->car_number;
        $cars->car_admin_id =  Auth::user()->id;
        $cars->save();
        return redirect()->back()->with('status', 'You have succuessfully added car!');
    }
}
