<?php

namespace App\Http\Controllers\CarService;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Caradmin;
use Illuminate\Support\Facades\Auth;

class CarServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        $passenger = Caradmin::where(['car_admin_id' => Auth::user()->id])->get();
        return view('carservice.dashboard', compact('passenger'));
    }
}
