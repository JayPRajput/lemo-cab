<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Models\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $admin = User::get();
        return view('admin.profile', compact('admin'));
    }


    public function store(Request $request)
    {
        return  redirect()->back();
    }

    public function edit($id)
    {
        $profileEdit = User::find($id);
        // dd($profileEdit);
        if ($profileEdit) {
            $admin = User::get();

            return view('admin.profile', compact('profileEdit', 'admin'));
        }
    }

    public function update(Request $request, $id)
    {
        // Logic for user upload of avatar
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move('uploads/profile/', $filename);
            $user = Auth::user();
            $user->image = $filename;
            $user->save();
        }

        User::where('id', $id)->update([
            'name' => $request['name'],
            'address' => $request['address'],
            'city' => $request['city'],
            'country' => $request['country'],
            'postelcode' => $request['postelcode'],
            'aboutme' => $request['aboutme'],
        ]);

        return redirect()->Route('admin.profile.index')->with('status', 'Profile has been updated SuccessFully');
    }

    public function deleteuser($id)
    {


        $dele = User::find($id);
        dd($dele);
    }
}
