<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {

        $driver = User::where('role_id', 4)->get();
        $carservice = User::where('role_id', 5)->get();
        $passenger = User::where(['role_id' => 3, 'admin_id' => Auth::user()->id])->get();
        $carservice =  User::where(['role_id' => 5, 'admin_id' => Auth::user()->id])->get();
        $driver = User::where(['role_id' => 4, 'admin_id' => Auth::user()->id])->get();
        return view('admin.dashboard', compact('passenger', 'driver', 'carservice'));
    }
}
