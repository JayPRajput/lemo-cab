<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


class CreateDriverController extends Controller
{
    public function index()
    {
        $roles = Role::select('role_id', 'role_name')->get();
        return view('admin.createdriver', compact('roles'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'role_id' => ['required'],
        ]);

        $admin =  new User();
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->role_id = $request->role_id;
        $admin->password = Hash::make($request['password']);
        $admin->admin_id = Auth::user()->id;

        $admin->save();

        $user = User::where('email', $request->email)->first();
        $user->notify(new VerifyEmail);
        return redirect()->Route('admin.createdriver.index')->with('status', 'You have  successfully create new User');
    }
}
