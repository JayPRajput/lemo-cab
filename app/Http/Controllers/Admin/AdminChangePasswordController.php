<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Rules\MatchOldPassword;
use Illuminate\Http\Request;

class AdminChangePasswordController extends Controller
{
    public function index()
    {
        return view('admin.password_update');
    }

    public  function  store(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'password' => ['required', 'string', 'confirmed', 'min:8'],
        ]);

        $password =    User::find(Auth::user()->id);
        // dd($password);
        $password->update([
            'password' => Hash::make($request->password)
        ]);
        return redirect()->route('admin.profile.index')->with('status', 'Password Has been changed');
    }
}
