<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Auth\Notifications\VerifyEmail;

class AdminprofileController extends Controller
{
    public function index()
    {
        $admin = User::get();
        return view('admin.profile', compact('admin'));
    }


    public function store(Request $request)
    {
        return  redirect()->back();
    }

    public function edit($id)
    {
        $profileEdit = User::find($id);
        // dd($profileEdit);
        if ($profileEdit) {
            $admin = User::get();

            return view('admin.profile', compact('profileEdit', 'admin'));
        }
    }

    public function update(Request $request, $id)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move('uploads/profile/', $filename);
            $user = Auth::user();
            $user->image = $filename;
            $user->save();
        }

        User::where('id', $id)->update([
            'name' => $request['name'],
            'address' => $request['address'],
            'city' => $request['city'],
            'country' => $request['country'],
            'postelcode' => $request['postelcode'],
            'aboutme' => $request['aboutme'],
        ]);

        if ($request->email) {
            $request->validate([
                'email' => ['email', 'string', 'max:255', 'unique:users'],
            ]);

            User::where('id', $id)->update([
                'email' => $request['email'],
                'email_verified_at' => NULL,

            ]);

            $user = User::where('email', $request->email)->first();
            // dd($user);
            $user->notify(new VerifyEmail);
            return redirect()->back()->with('status', 'We have send email verify link your mail . please check your email account!');
        }

        return redirect()->back()->with('status', 'Profile has been updated SuccessFully');
    }

    public function destroy($id)
    {
        $del  = User::find($id);
        $del->delete();
        return redirect()->Route('login')->with('status', 'Your Account deleted SuccessFully');
    }
}
