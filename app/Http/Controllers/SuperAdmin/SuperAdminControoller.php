<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SuperAdminControoller extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        $passenger =  User::where(['role_id' => 3, 'admin_id' => Auth::user()->id])->get();
        //  dd($passenger);
        $carservice =  user::where(['role_id' => 5, 'admin_id' => Auth::user()->id])->get();
        // dd($carservice);
        $driver =  user::where(['role_id' => 4, 'admin_id' => Auth::user()->id])->get();
        // dd($driver);
        return view('superadmin.dashboard', compact('passenger', 'driver', 'carservice'));
    }
}
