<?php

namespace App\Http\Controllers\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class PassengerController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    public function index()
    {
        $passenger = User::where('role_id', 3)->get();
        $driver = User::where('role_id', 4)->get();
        $carservice = User::where('role_id', 5)->get();
        // dd($carservice);

        // dd($passenger);
        return view('passenger.dashboard', compact('passenger', 'driver', 'carservice'));
    }
}
