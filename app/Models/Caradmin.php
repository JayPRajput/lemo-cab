<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Caradmin extends Model
{
    use HasFactory;
    public function roles()
    {
        return $this->belongsTo('App\User', 'car_admin_id', 'id');
    }
}
