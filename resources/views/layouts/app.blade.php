<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Lemo Cab booking ">
    <meta name="author" content="Lemo Cab">

    <title> @yield('title') - Lemo</title>

    <link rel="icon" href="{{ asset('/assets/img/favicon-lemo.png') }}" type="image/png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <link rel="stylesheet" href="{{ asset('/assets/vendor/nucleo/css/nucleo.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}"
        type="text/css">
    <link rel="stylesheet" href="{{ asset('/assets/css/argon.css?v=1.2.0') }}" type="text/css">

    <link rel="stylesheet" href="{{ asset('/build/css/intlTelInput.css') }}">
    <link rel="stylesheet" href="{{ asset('/build/css/demo.css') }}">
</head>

<body class="bg-default">

    <nav id="navbar-main" class="navbar navbar-horizontal navbar-transparent navbar-main navbar-expand-lg navbar-light">
        @include('auth.header')
    </nav>

    <div class="main-content">
        @yield('auth-content')

    </div>

    <script src="{{ asset('/assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/js-cookie/js.cookie.js') }}"></script>
    <script src="{{ asset('/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>

    <script src="{{ asset('/assets/vendor/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/chart.js/dist/Chart.extension.js') }}"></script>
    <script src="{{ asset('/assets/js/argon.js?v=1.2.0') }}"></script>
    <script src="{{ asset('/build/js/intlTelInput.js') }}"></script>
    <script src="{{ asset('/assets/js/validate.js') }}"></script>
    <script src="{{ asset('/build/js/utils.js') }}"></script>
    <script src="{{ asset('/assets/js/validate.js') }}"></script>
    <script src="{{ asset('/assest/js/profile.js') }}"></script>


    <script>
        var input = document.querySelector('#phone');
        var iti = window.intlTelInput(input, {
            // utilsScript: {{ asset('/build/js/utils.js') }}
        });
    </script>



</body>

</html>
