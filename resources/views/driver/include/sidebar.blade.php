<div class="scrollbar-inner">
    <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
            <h1>Lemo Cab</h1>
            {{-- <img src="assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
            --}}
        </a>
    </div>
    <div class="navbar-inner">
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" href="{{ url('driver/dashboard') }}">
                        <i class="ni ni-tv-2 text-primary"></i>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ Route('driver.profile.edit', Auth::user()->id) }}">
                        <i class="ni ni-single-02 text-yellow"></i>
                        <span class="nav-link-text">Profile</span>
                    </a>
                </li>

            </ul>

        </div>
    </div>
</div>
