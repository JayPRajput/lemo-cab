@extends('layouts.app')
@section('title', 'Register')


@section('auth-content')
    <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9 pad-top" style="padding-top: 2rem !important">
        <div class="container">
            <div class="header-body text-center mb-3">
                <div class="row justify-content-center">
                    <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                        <h1 class="text-white">Welcome!</h1>
                        <p class="text-lead text-white">Lemo Cab Booking</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="card bg-secondary border-0">
                    @if (Session::has('success'))
                        <p class="alert alert-info">{{ Session::get('success') }}</p>
                    @elseif(Session::has('error'))
                        <p class="alert alert-danger">{{ Session::get('error') }}</p>
                    @endif

                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Or sign up with credentials</small>
                        </div>
                        <form role="form" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                                    </div>
                                    <input id="name" onkeypress="return /^[a-zA-Z\s]+$/i.test(event.key)" type="text"
                                        class="form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ old('name') }}" placeholder="Name" required autocomplete="name" autofocus
                                        title="Enter alphabet Value">

                                    @error('name')
                                        <span class="invalid-feedback-form" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                                    </div>
                                    <select name="role_id" id="" value="{{ old('role_id') }}" required class="form-control"
                                        @error('role_id') is-invalid @enderror>
                                        <option value="">Select Your Role</option>
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->role_id }}"> {{ $role->role_name }}</option>
                                        @endforeach
                                    </select>

                                    @error('role_id')
                                        <span class="invalid-feedback-form" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input type="email" name="email"
                                        class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                                        placeholder="Email" required>
                                </div>
                                @error('email')
                                    <span class="invalid-feedback-form" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="input-group  input-group-alternative mb-3">
                                    <input type="tel" onkeypress="return /^-?\d*$/.test(event.key)" name="phone" id="phone"
                                        maxlength="14" required placeholder="+910123456789" value="{{ '+91' }}"
                                        class="form-control @error('phone') is-invalid @enderror"
                                        style="padding-right: 235px;">
                                </div>
                                <span id="valid-msg" class="hide"> Valid phone number</span>
                                <span id="error-msg" class="hide"></span>
                                @error('phone')
                                    <span class="invalid-feedback-form" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div id="message">
                                    <h5>Password must contain the following:</h5>
                                    <p id="letter" class="invalid"> <strong>A lowercase letter</strong> </p>
                                    <p id="capital" class="invalid"><strong>A capital (uppercase) letter</strong></p>
                                    <p id="number" class="invalid">A number</p>
                                    <p id="length" class="invalid">Minimum 8 characters</p>
                                </div>
                                <div class="input-group input-group-merge input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="new-password" placeholder="Password"
                                        value="{{ old('password') }}" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                        title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
                                </div>
                                @error('password')
                                    <span class="invalid-feedback-form" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input id="password-confirm" type="password" class="form-control"
                                        name="password_confirmation" required autocomplete="new-password"
                                        placeholder="Confirm Psassword" value="{{ old('password') }}">
                                </div>
                                @error('password_confirmation')
                                    <span class="invalid-feedback-form" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="text-center">
                                <input type="submit" class="btn btn-primary mt-4 btn-top" value="Create account">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
