@extends('layouts.app')

@section('title', 'Email-Verify')
@section('auth-content')

    <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9 pad-top" style="padding-top: 2rem !important">
        <div class="container">
            <div class="header-body text-center mb-3">
                <div class="row justify-content-center">
                    <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                        <h1 class="text-white">Welcome!</h1>
                        <p class="text-lead text-white">Lemo Cab Booking</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-7">
                <div class="card bg-secondary border-0 mb-0">

                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Before proceeding, please check your email for a verification link.</small>
                            <small>If you did not receive the email</small>
                        </div>
                        <form method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <div class="text-center">
                                <input type="submit" class="btn btn-primary my-4 btn-top"
                                    value="click here to request another">
                                <span class="btn btn-primary my-4 " style="color: red"><a href="{{ route('logout') }}"
                                        style="color: red">Exit</a></span>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
