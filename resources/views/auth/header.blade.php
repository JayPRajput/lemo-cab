 <!-- Navbar -->

 <div class="container">
     <a class="navbar-brand" href="{{ url('/') }}">
         {{-- <img src="../assets/img/brand/white.png"> --}}
         <h1>Lemo Cab Booking</h1>
     </a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse"
         aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
     </button>
     <div class="navbar-collapse navbar-custom-collapse collapse" id="navbar-collapse">
         <div class="navbar-collapse-header">
             <div class="row">
                 <div class="col-6 collapse-brand">
                     <a href="{{ url('/') }}">
                         {{-- <img src="../assets/img/brand/blue.png"> --}}
                         <h1>Lemo</h1>
                     </a>
                 </div>
                 <div class="col-6 collapse-close">
                     <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse"
                         aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                         <span></span>
                         <span></span>
                     </button>
                 </div>
             </div>
         </div>
         <ul class="navbar-nav mr-auto">

             <li class="nav-item">
                 <a href="{{ url('login') }}" class="nav-link">
                     <span class="nav-link-inner--text">Login</span>
                 </a>
             </li>
             <li class="nav-item">
                 <a href="{{ url('register') }}" class="nav-link">
                     <span class="nav-link-inner--text">Register</span>
                 </a>
             </li>
         </ul>

     </div>
 </div>
