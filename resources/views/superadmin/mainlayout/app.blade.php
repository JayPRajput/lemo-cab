<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Lemo Cab booking ">
    <meta name="author" content="Mr. Estno">

    <title> @yield('title') - Lemo</title>

    <link rel="icon" href="{{ asset('/assets/img/favicon-lemo.png') }}" type="image/png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <link rel="stylesheet" href="{{ asset('/assets/vendor/nucleo/css/nucleo.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}"
        type="text/css">
    <link rel="stylesheet" href="{{ asset('/assets/css/argon.css?v=1.2.0') }}" type="text/css">
</head>

<body>

    <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
        @include('superadmin.include.sidebar')
    </nav>

    <div class="main-content" id="panel">
        <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
            @include('superadmin.include.top_nav')
        </nav>
        <div class="header bg-primary pb-6">
            @include('superadmin.include.header')
        </div>
        <div class="container-fluid mt--6">
            @yield('content')

            <div class="footer-sec">
                @include('superadmin.include.footer')
            </div>
        </div>
    </div>

    <script src="{{ asset('/assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/js-cookie/js.cookie.js') }}"></script>
    <script src="{{ asset('/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>

    <script src="{{ asset('/assets/vendor/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/chart.js/dist/Chart.extension.js') }}"></script>
    <script src="{{ asset('/assets/js/argon.js?v=1.2.0') }}"></script>
</body>

</html>
