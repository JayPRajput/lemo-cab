@extends('superadmin.include.profile_layout')
@section('title', 'Superadmin-Profile')
@section('content')
    <div class="row">
        <div class="col-xl-4 order-xl-2">
            <div class="card card-profile">
                <img src="{{ asset('assets/img/theme/img-1-1000x600.jpg') }}" alt="Image placeholder" class="card-img-top">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image-user">
                            @if (Auth::user()->image == null)
                                <img src="{{ asset('assets/img/theme/avtar.jpg') }}" class="rounded-circle" alt=""
                                    srcset="">
                            @elseif (Auth::user())
                                <img src="{{ asset('uploads/profile/' . Auth::user()->image) }}" class="rounded-circle"
                                    title="" />
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                        <a href="#" class="btn btn-sm btn-info  mr-4 ">Connect</a>
                        <a href="#" class="btn btn-sm btn-default float-right">Message</a>
                    </div>
                </div>
                <div class="card-body pt-0">

                    <div class="text-center">
                        <h5 class="h3">
                            {{ Auth::user()->name }}<span class="font-weight-light"></span>
                        </h5>
                        <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i>{{ Auth::user()->email }}
                        </div>
                        <div class="h5 mt-4">
                            <i class="ni business_briefcase-24 mr-2"></i>{{ Auth::user()->aboutme }}
                        </div>
                        <div>
                            <i class="ni education_hat mr-2"></i>{{ Auth::user()->address }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 order-xl-1">
            @if (session('success'))
                <p class="alert text-success">{{ session('success') }}</p>
            @elseif(session('error'))
                <p class="alert text-danger">{{ session('error') }}</p>
            @endif
            <div class="card">
                <form
                    action="{{ Route::is('superadmin.profile.edit') ? Route('superadmin.profile.update', $profileEdit->id) : Route('superadmin.profile.index') }}"
                    method="post" enctype="multipart/form-data">
                    <div class="card-header">
                        @if (session('status'))
                            <p class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </p>
                        @endif
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ Auth::user()->name }} </h3>
                            </div>
                            <div class="col-4 text-right">
                                <input type="submit" value="update Profile" class="btn btn-sm btn-primary">

                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @csrf
                        @if (Route::is('superadmin.profile.edit'))
                            @method('PUT')
                        @endif
                        <h6 class="heading-small text-muted mb-4">User information</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-username">Name</label>
                                        <input type="text" name="name" id="input-username" class="form-control"
                                            placeholder="Username" value="{{ Auth::user()->name }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Email
                                            address</label>
                                        <input type="email" name="email" id="input-email" class="form-control"
                                            placeholder="jesse@example.com">
                                    </div>

                                    @error('email')
                                        <span class="invalid-feedback-form" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-first-name">
                                            Phone Number</label>
                                        <input type="text" name="phone" onkeypress="return /^-?\d*$/.test(event.key)"
                                            id="input-first-name" class="form-control" placeholder="First name"
                                            value="{{ Auth::user()->phone }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-last-name">Upload Profile
                                            Image</label>
                                        <input type="file" name="image" id="input-last-name" class="form-control"
                                            value="{{ Auth::user()->image }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-4" />
                        <!-- Address -->
                        <h6 class="heading-small text-muted mb-4">Contact information</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-address">Address</label>
                                        <input id="input-address" name="address" class="form-control"
                                            placeholder="Home Address" value="{{ Auth::user()->address }}" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-city">City</label>
                                        <input type="text" name="city" id="input-city" class="form-control"
                                            placeholder="City"
                                            value="{{ Route::is('superadmin.profile.edit') ? $profileEdit->city : Auth::user()->city }}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-country">Country</label>
                                        <input type="text" name="country" id="input-country" class="form-control"
                                            placeholder="Country" value="{{ Auth::user()->country }}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-country">Postal
                                            code</label>
                                        <input type="text" name="postelcode" onkeypress="return /^-?\d*$/.test(event.key)"
                                            maxlength="6" id="input-postal-code" class="form-control"
                                            placeholder="Postal code" value="{{ Auth::user()->postelcode }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-4" />
                        <!-- Description -->
                        <h6 class="heading-small text-muted mb-4">About me</h6>
                        <div class="pl-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">About Me</label>
                                <textarea rows="4" name="aboutme" class="form-control"
                                    placeholder="A few words about you ...">{{ Auth::user()->aboutme }}</textarea>
                            </div>
                        </div>

                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-4 text-left">
                                    <input type="submit" value="update Profile" class="btn btn-sm btn-primary">
                                </div>

                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
