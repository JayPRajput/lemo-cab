<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Lemo Cab booking ">
    <meta name="author" content="Mr. Estno">

    <title>Create new user - Lemo</title>

    <link rel="icon" href="{{ asset('/assets/img/favicon-lemo.png') }}" type="image/png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <link rel="stylesheet" href="{{ asset('/assets/vendor/nucleo/css/nucleo.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}"
        type="text/css">
    <link rel="stylesheet" href="{{ asset('/assets/css/argon.css?v=1.2.0') }}" type="text/css">
</head>

<body>
    <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
        @include('include.sidebar')
    </nav>
    <div class="main-content" id="panel">
        @include('include.profile_navbar')
        <div class="profile-header">
            @include('include.profile_header')
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col-xl-12 order-xl-1">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Create New User </h3>
                                </div>
                                <div class="col-4 text-right">
                                    <a href="#!" class="btn btn-sm btn-primary">Settings</a>
                                </div>
                                @if (session('status'))
                                    <p class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ url('admin/createdriver') }}" method="POST">
                                @csrf
                                <h6 class="heading-small text-muted mb-4">User information</h6>
                                <div class="pl-lg-4">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-username">
                                                    New User</label>
                                                <input type="text" id="input-username" class="form-control"
                                                    placeholder="Name" name="name" value="{{ old('name') }}">
                                            </div>

                                            @error('name')
                                                <span class="invalid-feedback-form" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-email">Email
                                                    address</label>
                                                <input type="email" id="input-email" name="email" class="form-control"
                                                    placeholder="jesse@example.com" value="{{ old('email') }}">
                                            </div>
                                            @error('email')
                                                <span class="invalid-feedback-form" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-first-name">
                                                    Select role</label>
                                                <select name="role_id" id="" value="{{ old('role_id') }}" required
                                                    class="form-control" @error('role_id') is-invalid @enderror>
                                                    <option value="">Select New User</option>

                                                    <option value="3" class="form-control">Passenger</option>
                                                    <option value="4" class="form-control">Driver</option>
                                                    <option value="5" class="form-control">Car service Provider</option>

                                                </select>

                                                @error('role_id')
                                                    <span class="invalid-feedback-form" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-last-name">Password
                                                </label>
                                                <input type="password" name="password" id="input-last-name"
                                                    class="form-control" placeholder="Password" value="Jesse">
                                            </div>
                                            @error('password')
                                                <span class="invalid-feedback-form" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div class="col-4 text-left">
                                                    <input type="submit" value="create User"
                                                        class="btn btn-xm btn-primary">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-sec">
                @include('include.footer')
            </div>
        </div>
    </div>

    <script src="{{ asset('/assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/js-cookie/js.cookie.js') }}"></script>
    <script src="{{ asset('/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>

    <script src="{{ asset('/assets/vendor/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/chart.js/dist/Chart.extension.js') }}"></script>
    <script src="{{ asset('/assets/js/argon.js?v=1.2.0') }}"></script>
    <script src="{{ asset('/assets/js/profile.js') }}"></script>

</body>

</html>
