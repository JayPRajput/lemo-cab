@extends('include.profile_layout')
@section('title', 'Password-Update')
@section('content')
    <div class="row">
        <div class="col-xl-4 order-xl-2">
            <div class="card card-profile">
                <img src="{{ asset('assets/img/theme/img-1-1000x600.jpg') }}" alt="Image placeholder" class="card-img-top">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image-user">
                            @if (Auth::user()->image == null)
                                <img src="{{ asset('assets/img/theme/avtar.jpg') }}" class="rounded-circle" alt=""
                                    srcset="">
                            @elseif (Auth::user())
                                <img src="{{ asset('uploads/profile/' . Auth::user()->image) }}" class="rounded-circle"
                                    title="" />
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                        <a href="#" class="btn btn-sm btn-info  mr-4 ">Connect</a>
                        <a href="#" class="btn btn-sm btn-default float-right">Message</a>
                    </div>
                </div>
                <div class="card-body pt-0">

                    <div class="text-center">
                        <h5 class="h3">
                            {{ Auth::user()->name }}<span class="font-weight-light"></span>
                        </h5>
                        <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i>{{ Auth::user()->email }}
                        </div>
                        <div class="h5 mt-4">
                            <i class="ni business_briefcase-24 mr-2"></i>{{ Auth::user()->aboutme }}
                        </div>
                        <div>
                            <i class="ni education_hat mr-2"></i>{{ Auth::user()->address }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 order-xl-1">
            @if (session('success'))
                <p class="alert text-success">{{ session('success') }}</p>
            @elseif(session('error'))
                <p class="alert text-danger">{{ session('error') }}</p>
            @endif
            <div class="card">
                <form method="POST" action="{{ url('admin/password_update') }}">
                    <div class="card-header">
                        @if (session('status'))
                            <p class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </p>
                        @endif
                        @csrf
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ Auth::user()->name }} </h3>
                            </div>
                            <div class="col-4 text-right">
                                <input type="submit" class="btn btn-sm btn-primary" value="Update Password">
                            </div>
                        </div>
                    </div>
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="form-group mb-3">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                </div>
                                <input id="password_confirm" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="current_password"
                                    required autocomplete="new-password" placeholder="Password">
                            </div>
                            @error('current_password')
                                <span class="invalid-feedback-form" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                        <div class="form-group mb-3">
                            <div id="message">
                                <h3>Password must contain the following:</h3>
                                <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
                                <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
                                <p id="number" class="invalid">A <b>number</b></p>
                                <p id="length" class="invalid">Minimum <b>8 characters</b></p>
                            </div>
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                </div>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password" required
                                    autocomplete="new-password" placeholder="Password">
                            </div>
                            @error('password')
                                <span class="invalid-feedback-form" role="alert">
                                    <strong>{{ $message }}</strong>
                                @enderror
                        </div>

                        <div class="form-group mb-3">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                </div>
                                <input id="password_confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password"
                                    placeholder="Confirm Password">
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
