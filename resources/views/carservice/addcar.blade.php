@extends('carservice.include.profile_layout')
@section('title', 'Add-Car')


@section('content')
    <div class="row">
        <div class="col-xl-4 order-xl-2">
            <div class="card card-profile">
                <img src="{{ asset('assets/img/theme/img-1-1000x600.jpg') }}" alt="Image placeholder" class="card-img-top">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image-user">
                            @if (Auth::user()->image == null)
                                <img src="{{ asset('assets/img/theme/avtar.jpg') }}" class="rounded-circle" alt=""
                                    srcset="">
                            @elseif (Auth::user())
                                <img src="{{ asset('uploads/profile/' . Auth::user()->image) }}" class="rounded-circle"
                                    title="" />
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                        <a href="#" class="btn btn-sm btn-info  mr-4 ">Connect</a>
                        <a href="#" class="btn btn-sm btn-default float-right">Message</a>
                    </div>
                </div>
                <div class="card-body pt-0">

                    <div class="text-center">
                        <h5 class="h3">
                            {{ Auth::user()->name }}<span class="font-weight-light"></span>
                        </h5>
                        <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i>{{ Auth::user()->email }}
                        </div>
                        <div class="h5 mt-4">
                            <i class="ni business_briefcase-24 mr-2"></i>{{ Auth::user()->aboutme }}
                        </div>
                        <div>
                            <i class="ni education_hat mr-2"></i>{{ Auth::user()->address }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 order-xl-1">
            @if (session('success'))
                <p class="alert text-success">{{ session('success') }}</p>
            @elseif(session('error'))
                <p class="alert text-danger">{{ session('error') }}</p>
            @endif
            <div class="card">
                <form action="{{ url('carservice/addcar') }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="card-header">
                        @if (session('status'))
                            <p class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </p>
                        @endif
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ Auth::user()->name }} </h3>
                            </div>
                            <div class="col-4 text-right">
                                <input type="submit" value="Add Car" class="btn btn-sm btn-primary">
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h6 class="heading-small text-muted mb-4">User information</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-username">Car Name</label>
                                        <input type="text" name="car_name" id="input-username" class="form-control"
                                            placeholder="Enter Car name" value="{{ old('car_name') }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Car Model
                                        </label>
                                        <input type="text" name="car_model" id="input-email" class="form-control"
                                            placeholder="Enter car model">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-first-name">
                                            Car Number</label>
                                        <input type="text" name="car_number" class="form-control"
                                            placeholder="Rj 05 cv 1920" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-last-name">Upload Car
                                            Image</label>
                                        <input type="file" name="car_image" id="input-last-name" name="profile_image"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
