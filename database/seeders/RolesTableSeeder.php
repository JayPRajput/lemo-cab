<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('roles')->insert([
           'role_name' => 'Super Admin'
       ]);

        DB::table('roles')->insert([
            'role_name' => 'Admin',
        ]);

        DB::table('roles')->insert([
            'role_name' => 'Passenger',

        ]);

        DB::table('roles')->insert([
            'role_name' => 'Drivers',
        ]);

        DB::table('roles')->insert([
            'role_name' => 'Car Service Providers',
        ]);
    }
}
