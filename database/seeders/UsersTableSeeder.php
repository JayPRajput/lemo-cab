<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'Super Admin',
            'image' => 'null',
            'phone' => '123456789',
            'email' => 'super@gmail.com',
            'password' => bcrypt('pass@super'),
        ]);

        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'Admin',
            'image' => 'null',
            'phone' => '123456789',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('pass@admin'),
        ]);

        DB::table('users')->insert([
            'role_id' => '3',
            'name' => 'Passenger',
            'image' => 'null',
            'phone' => '123456789',
            'email' => 'Passenger@gmail.com',
            'password' => bcrypt('pass@Passenger'),
        ]);

        DB::table('users')->insert([
            'role_id' => '4',
            'name' => 'Driver',
            'image' => 'null',
            'phone' => '123456789',
            'email' => 'driver@gmail.com',
            'password' => bcrypt('pass@driver'),
        ]);
        DB::table('users')->insert([
            'role_id' => '5',
            'name' => 'Car service Provider',
            'image' => 'null',
            'phone' => '123456789',
            'email' => 'Car@gmail.com',
            'password' => bcrypt('pass@car'),
        ]);
    }
}
