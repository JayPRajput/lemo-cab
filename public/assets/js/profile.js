function testInput(event) {
    var value = String.fromCharCode(event.which);
    var pattern = new RegExp(/[a-zåäö ]/i);
    return pattern.test(value);
}

$('#input-username').bind('keypress', testInput);
$('#input-country').bind('keypress', testInput);
$('#input-city').bind('keypress', testInput);
$('#input-city').bind('keypress', testInput);
$('#aboutme').bind('keypress', testInput);


var x = document.getElementById('search');
x.addEventListener('keypress', searchFunction);

function searchFunction() {
    var input, filter, table_value, li, a, i, txtValue;
    input = document.getElementById('search');
    filter = input.value.toUpperCase();
    table_value = document.getElementById('th');
    li = table_value.getElementsByTagName("tr");
    console.log('tr value check', li);

    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("th")[0];
        txtValue = a.textContent || a.innerText;
        console.log('txtValue check', txtValue);
        var check = txtValue.toUpperCase().indexOf(filter) > -1;
        console.log('cheking value is', check);
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
